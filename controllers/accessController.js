'use strict'

const requestJson = require('request-json');
const security =require('../security/security');
const config=require('../config');
const data=require('../util/util')
const url=config.urlMLAB;

//Login
function login(request,response){
    console.log("Login...");

    var client = requestJson.createClient(url);
    const queryName='q={"numeroDocumento":"'+request.body.numeroDocumento+'"}&';
    client.get(config.mlab_users+'?'+queryName+config.apiKey, function(errG, respG, bodyG) {
    if(bodyG.length>0){
          var respuesta=bodyG[0];
          if(request.body.password==respuesta.password){
            var session = {
              "session" : true
            };
            var change = '{"$set":' + JSON.stringify(session) + '}';
            client.put(config.mlab_users+'?q={"numeroDocumento": ' + respuesta.numeroDocumento + '}&' + config.apiKey, JSON.parse(change), function(errP, resP, bodyP) {
            response.status(resP.statusCode).send({token:security.createToken(respuesta.numeroDocumento),user:respuesta});
            })
          }else{
            response.status(data.httpCodes.invalidData).send(data.msgInvalidData);
          }
      }else{
        response.status(data.httpCodes.invalidData).send(data.msgInvalidData);
    }
  });
}

//logout
function logout(request,response){
    console.log("Loguot");

    var client = requestJson.createClient(url);
    const queryName='q={"numeroDocumento":"'+request.body.numeroDocumento+'"}&';
    client.get(config.mlab_users+'?'+queryName+config.apiKey, function(err, res, bodyG) {
      if(bodyG.length>0){
        var respuesta=bodyG[0];
        var session = {
          "session" : false
        };
        var change = '{"$set":' + JSON.stringify(session) + '}';
        client.put(config.mlab_users+'?q={"numeroDocumento": ' + respuesta.numeroDocumento + '}&' + config.apiKey, JSON.parse(change), function(errP, resP, bodyP) {
          response.status(resP.statusCode).send(data.msgLogoutSuccess)
        });
      }else{
        response.status(data.httpCodes.invalidData).send(data.msgInvalidData);
      }
    });
}

module.exports={
  login,
  logout
};
