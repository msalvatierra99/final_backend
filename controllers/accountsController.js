var requestJson = require('request-json');
const config=require('../config');
const url=config.urlMLAB;

//Lista de Cuentas por cliente
function getAccounts(request,response){
  console.log("Listar cuentas por cliente");
  var client=requestJson.createClient(url);
  var query = 'q={"cliente":"' + request.params.id + '"}&';
  client.get(config.mlab_account+'?'+query+config.apiKey,function(err,req,body){
    if(err){
      console.log(err);
    }
    else {
    response.send(body);
    }
  })
}

function createAccount(request,response){
    console.log("Crear cuenta");
    var client=requestJson.createClient(url);
    var aleatorio = Math.floor(Math.pow(10, 8) + Math.random() * 9 * Math.pow(10, 8));
    var cta="001104860"+aleatorio;
    console.log("numeroCuenta: "+cta);
    var newCta={
              "cliente":request.body.cliente ,
              "numeroCuenta":request.body.numeroCuenta,
              "tipoCuenta":request.body.tipoCuenta ,
              "moneda":request.body.moneda,
              "saldoContable":request.body.saldoDisponible,
              "saldoDisponible":request.body.saldoDisponible,
              "movimientos": []
            };
    client.post(config.mlab_account+'?'+config.apiKey,newCta,function(err,resp,body){
      if(err){
        console.log(err);
      }else {
        response.send(body);
      }
    })
}

module.exports={
getAccounts,
createAccount
}
