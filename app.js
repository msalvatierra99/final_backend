const express = require('express');
/*const validator = require('express-validator');
*/
const bodyParser = require('body-parser');
const config = require('./config');
const api = require('./routes/route');

const app=express();
/*app.use(validator());
*/
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(config.URLbase,api);
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});


module.exports=app;
