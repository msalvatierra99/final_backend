'use strict'

const app=require('./app');
const config=require('./config');

app.listen(config.port,()=>{
  console.log('Backend port running...'+config.port);
});
