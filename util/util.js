module.exports = {
    queryLoged : '{"$set":{"logged":true}}',
    queryLogout : '{"$unset":{"logged":true}}',
    filter : {
      users     : 'f={"_id":0}&l=2000&',
      accounts  : 'f={ "_id":0,  "accounts.movements" : 0,"usr_type_id_card":0 ,"usr_id_card":0,"usr_password":0,"usr_phone":0,"usr_ip_address":0, "usr_name": 0, "usr_last_name":0}&'
    },
    msgError : {
      'msg': 'Error'
    },
    msgEmpty :{
      'msg' : 'No se encontraron registros'
    },
    msgInvalidData :{
      'msg' : 'Datos inválidos'
    },
    msgUserNotLoged :{
      'msg' : 'Usuario no logeado'
    },
    existUser :{
      'msg' : 'Usuario ya existente'
    },
    msgSuccessModify:{
      'msg' : 'Registro modificado correctamente'
    },
    msgSuccessDelete:{
      'msg' : 'Registro eliminado correctamente'
    },
    msgAddAccountSuccess:{
      'msg' : 'Cuenta agregada satisfactoriamente'
    },
    msgAddMovementSuccess:{
      'msg' : 'Movimiento agregado satisfactoriamente'
    },
    msgLogoutSuccess:{
      'msg' : 'Logout satisfactorio, vuelva pronto'
    },
    httpCodes : {
      registered  : '409',
      invalidData : '404'
    },

    authSucess : {
      'msg' : 'Autenticacion valida'
    }
}
