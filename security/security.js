'use strict'

const jwt=require('jwt-simple');
const moment=require('moment');
const config=require('../config');
const data=require('../util/util');

  function createToken(id){
    const payload={
      sub: id,
      iat: moment().unix(),
      iat: moment().add(14,'days').unix(),
    };
    return jwt.encode(payload,config.SECRET_TOKEN);
  }

  function decodeToken(token){
    console.log("decodeToken");
      const decode=new Promise((resolve,reject)=>{
      try{
        const payload= jwt.decode(token,config.SECRET_TOKEN);
        if(payload.exp < moment().unix){
        reject({
          status:401,
          message: 'Token ha Expirado'
        })
        }
        resolve(payload.sub);
      }catch (err){
        reject({
          status:500,
          message: 'Invalid token'
        })
      }
  });
  return decode;
  }

  function isAuth(req, res,next){
    if(!req.headers.authorization)
      return res.status(403).send(data.msgUserNotLoged);
      const token   = req.headers.authorization.split(" ")[1];
      decodeToken(token)
        .then(response=>{
          req.user=response;
          next();
        })
        .catch(response=>{
          res.status(response.status).send(response.message);
        })
    }

module.exports={
  createToken,
  decodeToken,
  isAuth
};
